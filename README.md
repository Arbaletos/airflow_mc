# Airflow_MC

Source code for Airflow Tutorial in MLOps course.
https://www.youtube.com/watch?v=ZXF25MxIE-k

In this masterclass two proposed by creator of Airflow ways of deploying it in testing mode were demonstrated: locally (directory ubuntu) and as a docker.  

Also, basic usecases of MongoDB were demonstrated as well.

The only prerequisutes is a docker-compose 1.29.1 or greater -- all required packages and environments can be achived via Docker.  

To test Database-connecting dags we need to launch MongoDB container -- launch script is situated in mongo/run_docker.bat

## Starting Airflow locally 

Main  article: https://airflow.apache.org/docs/apache-airflow/stable/start/local.html

Root directory for this mode is ubuntu/.  Because of Airflow won't work locally in Windows, we use docker-image of ubuntu here (ubuntu/start_docker), and installs Airflow inside it. Configure -v option is start_docker file to match your system!

To do so, when we get inside ubuntu container, we need to launch init_scipts/env.sh (to export airflow_home env variable), than init_scripts/install.sh to upgrade pip and install Airflow, than init_scripts/init.sh to initialize SQLite Database and create basic user (you can change credentials of this user as you wish).  

After these steps the configuration is ended, and time comes to launch server (init_scripts/server.sh), and scheduler (init_scipts/scheduler.sh). Two launch two these services in one container you can use tmux or connect the same container from another terminal. Don't forget to import airflow_home in second window as well!

After this, Airflow WEB-GUI should be accesible.

## Starting Airflow in docker

Main  article: https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html
Everything is much more simple, because you need only to init database (docker-compose up airflow-init) and launch images (docker-compose up), and everything should work. This deployment is much more complex, than former, so use it only if you understands, what going on.

## Proposed dags

In this repository, in addition to Dags, proposed by Airflow team by default, there are two additional dags: hell2u (simple helloworld-like dag) and mongo_datastamp, which illustrates connection to external resources. Proposed variants of connection are very basic and insecure, so don't use the same in production.
Please note, what MongoDB server, launched in a docker, is accessible on your local machine on localhost, but airflow instances inside docker containers can see it only if you add MongoDB container into corresponding docker network.  

Good Luck!  
