import datetime
import subprocess

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator

from airflow.decorators import task
import pendulum

import os
from pymongo import MongoClient
import glob
import json

import subprocess

with DAG(
    dag_id='mongo_stamp',
    schedule_interval='0 0 * * *', #at 0 0 every day
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    dagrun_timeout=datetime.timedelta(hours=6),
    tags=['mongo'],
    params={'db_name': 'data',
            'conn': 'mongodb://172.17.0.3:27017/',
            'collection': 'connections'}
) as dag:

    params = {k:dag.params[k] for k in dag.params}

    start = DummyOperator(task_id='start')
    
    @task(task_id=f"update_params")
    def update_params(**kwargs):
        print(params)
        print(kwargs['dag_run'].conf)
        params.update(kwargs['dag_run'].conf)
        return params
    
    @task(task_id='print_kwargs')
    def print_kwargs(**kwargs):
        print(kwargs)
    
    @task(task_id='mongo_stamp')
    def mongo_datastamp(ti=None, **kwargs):
        print('Init mongo')
        
        params = ti.xcom_pull(task_ids='update_params', key='return_value')
        
        print(params)
        
        conn = params['conn']
        db_name = params['db_name']
        collection = params['collection']
    
        db = MongoClient(conn)[db_name] 
        
        db[collection].insert_one({"timestamp": kwargs['ts'], "client": "ubuntu"})

    start >> update_params() >> mongo_datastamp()

if __name__ == "__main__":
    dag.cli()