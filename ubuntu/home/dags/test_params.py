import datetime
import subprocess

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator


from airflow.decorators import task
import pendulum

import os

import subprocess

with DAG(
    dag_id='test_params',
    schedule_interval=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['raw_store'],
    params={'key':'val'},
) as dag:    

    start = DummyOperator(task_id='start')
    
    params = {k:dag.params[k] for k in dag.params}

    @task(task_id=f"update_params")
    def update_params(**kwargs):
        print(params)
        print(kwargs['dag_run'].conf)
        params.update(kwargs['dag_run'].conf)
        return params
        
    bop = BashOperator(
        task_id='echo_param',
        bash_command='echo "key is {{ ti.xcom_pull(key="return_value", task_ids="update_params")["key"] }}"'
    )
        
    start >> update_params() >> bop
    
    

if __name__ == "__main__":
    dag.cli()