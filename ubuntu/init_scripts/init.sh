airflow db init

airflow users create \
    --username airflow \
    --firstname Aang \
    --lastname LastAirflowBender \
    --role Admin \
    --email aang_airflow_bender@gmail.at
