import datetime
import subprocess

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator


from airflow.decorators import task
import pendulum

import os

import subprocess

with DAG(
    dag_id='hell2u',
    schedule_interval='0 0 * * *',
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['tag1'],
    params={'key':'val'},
) as dag:    

    start = DummyOperator(task_id='start')
    
    @task(task_id=f"hell2u_python")
    def hell2u_py(**kwargs):
        print('Hell2U!')
        
    hell2u_bash = BashOperator(
        task_id='hell2u_bash',
        bash_command='echo "Hell2U!"'
    )
        
    start >> hell2u_py() >> hell2u_bash
    
    

if __name__ == "__main__":
    dag.cli()