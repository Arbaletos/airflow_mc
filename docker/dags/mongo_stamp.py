import datetime
import subprocess

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.decorators import task
from airflow.models import Variable
import pendulum

import os
import glob
import shutil
import json

import subprocess

with DAG(
    dag_id='mongo_stamp',
    schedule_interval='0 0 * * *', #at 0 0 every day
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    dagrun_timeout=datetime.timedelta(hours=6),
    tags=['mongo'],
) as dag:

    
    start = DummyOperator(task_id='start')
    
    if not shutil.which("virtualenv"):
        log.warning("The virtalenv_python example task requires virtualenv, please install it.")
    else:
        @task.virtualenv(task_id='mongo_stamp', requirements = ["pymongo"], system_site_packages=False)
        def mongo_datastamp(conn, **kwargs):
            from pymongo import MongoClient
            
            db_name = 'data' 
            
            print('Init mongo')
            db = MongoClient(conn)[db_name] 
            
            db['connections'].insert_one({"timestamp": kwargs['ts'], "client": "docker"})

        #start >> mongo_datastamp(Variable.get("mongodb_conn"))  # Dynamic load via Variables section in GUI.
        start >> mongo_datastamp('mongodb://172.17.0.3:27017/')

if __name__ == "__main__":
    dag.cli()